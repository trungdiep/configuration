from django.test import TestCase
from polls.models import Question
from django.utils import timezone
# Create your tests here.


class TestCase1(TestCase):
    def setUp(self) -> None:
        Question.objects.create(question_text='what"s up', pub_date=timezone.now())

    def test_simple(self):
        self.assertEqual(1, 1)

    def test_first_question(self):
        q = Question.objects.first()
        self.assertEqual(q.pk, 1)
